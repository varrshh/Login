import React, {useState} from "react";
import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import { Login } from './Components/Login';
import  { Register }  from './Components/Register';
import Dashboard from './Components/DashBoard';

function App() {

  return (
    
    <div>
 
      <Router>
        <Routes>
          <Route>
        <Route path='/' element={<Dashboard /> } />
            <Route path='/Dashboard' element={( <Dashboard />)} />
        
            </Route>
            <Route path='/login' element={(<Login />)} ></Route>
            <Route path='/Register' element={(<Register />)} ></Route>
        </Routes>

      </Router>
    </div>
  );
}

export default App;
