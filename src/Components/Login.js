import React,{useState} from 'react'
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Container, Row, Col } from 'react-bootstrap';
import { Link, useNavigate, useLocation } from 'react-router-dom';


export const Login = () => {
    const [email, setEmail]=useState('');
    const[pass,setPass]=useState('');
    const handleSubmit=() => {
       
        console.log(email);
    }
  return (
    <div className="main">
  <form onSubmit ={handleSubmit}>
    <label for="email">Email</label>
    <input value={email}type="email" placeholder="Enter your email" id="youremail@gmail.com" name='email'/>
    <label for="password">Password</label>
    <input value={pass} type="password" placeholder="********" id="password" name='password'/>
    <button type="submit"> Log In </button>
  </form>
  <button> Don't have an account? Register here.</button>
  </div>
  )
}
